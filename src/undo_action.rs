use news_flash::models::{CategoryID, FeedID, TagID};
use serde::{Deserialize, Serialize};
use std::fmt;

#[derive(Hash, Eq, Clone, Debug, Serialize, Deserialize)]
pub enum UndoAction {
    DeleteFeed(FeedID, String),
    DeleteCategory(CategoryID, String),
    DeleteTag(TagID, String),
}

impl fmt::Display for UndoAction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            UndoAction::DeleteFeed(id, label) => write!(f, "Delete Feed '{}' (id: {})", label, id),
            UndoAction::DeleteCategory(id, label) => write!(f, "Delete Category '{}' (id: {})", label, id),
            UndoAction::DeleteTag(id, label) => write!(f, "Delete Tag '{}' (id: {})", label, id),
        }
    }
}

impl PartialEq for UndoAction {
    fn eq(&self, other: &Self) -> bool {
        match self {
            UndoAction::DeleteFeed(self_id, _self_title) => match other {
                UndoAction::DeleteFeed(other_id, __other_title) => self_id == other_id,
                UndoAction::DeleteCategory(_other_id, __other_title) => false,
                UndoAction::DeleteTag(_other_id, __other_title) => false,
            },
            UndoAction::DeleteCategory(self_id, _title) => match other {
                UndoAction::DeleteFeed(_other_id, __other_title) => false,
                UndoAction::DeleteCategory(other_id, __other_title) => self_id == other_id,
                UndoAction::DeleteTag(_other_id, __other_title) => false,
            },
            UndoAction::DeleteTag(self_id, _title) => match other {
                UndoAction::DeleteFeed(_other_id, __other_title) => false,
                UndoAction::DeleteCategory(_other_id, __other_title) => false,
                UndoAction::DeleteTag(other_id, __other_title) => self_id == other_id,
            },
        }
    }
}
