use super::error::{LoginScreenError, LoginScreenErrorKind};
use super::LoginPrevPage;
use crate::app::{Action, App};
use crate::i18n::i18n;
use crate::util::{GtkUtil, Util};
use glib::{clone, subclass, SignalHandlerId};
use gtk4::{prelude::*, subclass::prelude::*, Box, Button, CompositeTemplate};
use libadwaita::{Toast, ToastOverlay};
use news_flash::models::{ApiSecret, LoginData, LoginGUI, OAuthData, PluginInfo};
use news_flash::{NewsFlashError, NewsFlashErrorKind};
use parking_lot::RwLock;
use std::sync::Arc;
use webkit2gtk::{traits::WebViewExt, LoadEvent, WebContext, WebView};

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/web_login.ui")]
    pub struct WebLogin {
        #[template_child]
        pub back_button: TemplateChild<Button>,
        #[template_child]
        pub toast_overlay: TemplateChild<ToastOverlay>,
        #[template_child]
        pub main_box: TemplateChild<Box>,

        pub redirect_signal_id: Arc<RwLock<Option<SignalHandlerId>>>,

        pub webview: Arc<RwLock<Option<WebView>>>,
        pub custom_api_secret: RwLock<Option<ApiSecret>>,
        pub prev_page: RwLock<Option<LoginPrevPage>>,
    }
    impl Default for WebLogin {
        fn default() -> Self {
            Self {
                back_button: TemplateChild::default(),
                toast_overlay: TemplateChild::default(),
                main_box: TemplateChild::default(),

                redirect_signal_id: Arc::new(RwLock::new(None)),

                webview: Arc::new(RwLock::new(None)),
                custom_api_secret: RwLock::new(None),
                prev_page: RwLock::new(None),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for WebLogin {
        const NAME: &'static str = "WebLogin";
        type ParentType = gtk4::Box;
        type Type = super::WebLogin;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for WebLogin {}

    impl WidgetImpl for WebLogin {}

    impl BoxImpl for WebLogin {}
}

glib::wrapper! {
    pub struct WebLogin(ObjectSubclass<imp::WebLogin>)
        @extends gtk4::Widget, gtk4::Box;
}

impl WebLogin {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[])
    }

    pub fn init(&self) {
        let imp = imp::WebLogin::from_instance(self);

        let context = WebContext::default().expect("Failed to create WebContext");
        let webview = WebView::with_context(&context);
        webview.set_hexpand(true);
        webview.set_vexpand(true);
        imp.main_box.append(&webview);

        imp.webview.write().replace(webview);
    }

    pub fn set_custom_api_secret(&self, custom_api_secret: Option<&ApiSecret>) {
        self.imp().custom_api_secret.write().take();
        if let Some(custom_api_secret) = custom_api_secret {
            self.imp().custom_api_secret.write().replace(custom_api_secret.clone());
        }
    }

    pub fn show_error(&self, error: NewsFlashError) {
        let imp = imp::WebLogin::from_instance(self);

        let error_text = match error.kind() {
            NewsFlashErrorKind::Login => i18n("Failed to log in"),
            _ => i18n("Unknown error."),
        };

        let toast = Toast::new(&error_text);
        toast.set_button_label(Some("details"));
        toast.set_action_name(Some("win.show-error-dialog"));
        imp.toast_overlay.add_toast(&toast);

        App::default().set_newsflash_error(error);
    }

    pub fn set_service(&self, info: &PluginInfo, prev_page: LoginPrevPage) -> Result<(), LoginScreenError> {
        let imp = imp::WebLogin::from_instance(self);

        imp.prev_page.write().replace(prev_page);

        let custom_api_secret = imp.custom_api_secret.read().clone();
        if let LoginGUI::OAuth(web_login_desc) = &info.login_gui {
            if let Some(url) = (web_login_desc.login_website)(custom_api_secret.as_ref()) {
                let webview = imp
                    .webview
                    .read()
                    .clone()
                    .ok_or::<LoginScreenError>(LoginScreenErrorKind::Unknown.into())?;

                webview.load_uri(url.as_str());
                let plugin_id = info.id.clone();
                let redirect_url = web_login_desc.catch_redirect.clone();
                let signal_id = webview.connect_load_changed(clone!(
                    @weak imp.redirect_signal_id as redirect_signal_id,
                    @strong custom_api_secret => @default-panic, move |webview, event|
                {
                    match event {
                        LoadEvent::Started | LoadEvent::Redirected => {
                            if let Some(redirect_url) = &redirect_url {
                                if let Some(uri) = webview.uri() {
                                    if uri.len() > redirect_url.len() && &uri[..redirect_url.len()] == redirect_url {
                                        let oauth_data = OAuthData {
                                            id: plugin_id.clone(),
                                            url: uri.as_str().to_owned(),
                                            custom_api_secret: custom_api_secret.clone(),
                                        };
                                        let oauth_data = LoginData::OAuth(oauth_data);
                                        GtkUtil::disconnect_signal(redirect_signal_id.write().take(), webview);
                                        webview.stop_loading();
                                        Util::send(Action::Login(oauth_data));
                                    }
                                }
                            }
                        }
                        _ => {
                            // do nothing
                        }
                    }
                }));

                imp.redirect_signal_id.write().replace(signal_id);
                return Ok(());
            }

            return Err(LoginScreenErrorKind::OauthUrl.into());
        }

        Err(LoginScreenErrorKind::LoginGUI.into())
    }

    pub fn reset(&self) {
        self.imp().webview.read().as_ref().map(|w| w.load_plain_text(""));
    }

    pub fn show(&self) {
        self.imp().webview.read().as_ref().map(|w| w.show());
    }

    pub fn connect_back<F: Fn(Option<&LoginPrevPage>) + 'static>(&self, f: F) {
        // setup back button to turn to previous page
        self.imp()
            .back_button
            .connect_clicked(clone!(@weak self as this => @default-panic, move |_button| {
                f(this.imp().prev_page.read().as_ref());
            }));
    }
}
