use news_flash::models::{ArticleID, Marked, Read};
use std::sync::Arc;

#[derive(Debug, Clone)]
pub struct ReadUpdate {
    pub article_id: Arc<ArticleID>,
    pub read: Read,
}

#[derive(Debug, Clone)]
pub struct MarkUpdate {
    pub article_id: Arc<ArticleID>,
    pub marked: Marked,
}
